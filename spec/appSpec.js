/* Jasmine Unit Testing - appSpec */
/* (Unit tests for all functions in app.js) */

describe('styleSizeByImportance', function() {

	var defaultSizeObj = {
			"height" : "64px",
			"width" : "64px"
			};

	it('Should return a default height & width *size* object', function() {
		var obj = styleSizeByImportance();

		expect(obj).toEqual(
			{
			"height" : "64px",
			"width" : "64px"
			});
	});


	it('Should scale output height & width by percentage of' +
	 ' input parameter *importance*', function() {
		
		/* set defaults */
		var dSize = 64;

		var importance0 = 0; // 0% of default size
		var importance1 = 25; // 25% of default size
		var importance2 = 50; // 50% of default size
		var importance3 = 75; // 75% of default size
		var importance4 = 100; // 100% of default size

		var size0 = (dSize * importance0) / 100;
		var size1 = (dSize * importance1) / 100;
		var size2 = (dSize * importance2) / 100;
		var size3 = (dSize * importance3) / 100;
		var size4 = (dSize * importance4) /100;

		var obj0 = styleSizeByImportance(0);
		var obj1 = styleSizeByImportance(25);
		var obj2 = styleSizeByImportance(50);
		var obj3 = styleSizeByImportance(75);
		var obj4 = styleSizeByImportance(100);

		/* set up test objects */
		var tObj0 = {
					  "height" : (size0 + "px"),
					  "width"  : (size0 + "px")
					};

		var tObj1 = {
					  "height" : (size1 + "px"),
					  "width"  : (size1 + "px")
					};

		var tObj2 = {
					  "height" : (size2 + "px"),
					  "width"  : (size2 + "px")
					};

		var tObj3 = {
					  "height" : (size3 + "px"),
					  "width"  : (size3 + "px")
					};

		var tObj4 = {
					  "height" : (size4 + "px"),
					  "width"  : (size4 + "px")
					};

		expect(obj0).toEqual(tObj0);
		expect(obj1).toEqual(tObj1);
		expect(obj2).toEqual(tObj2);
		expect(obj3).toEqual(tObj3);
		expect(obj4).toEqual(tObj4);
	});

	it('Should overwrite default size if size parameter is supplied', function() {

		var size = 100;
		var importance = 100;

		var obj = styleSizeByImportance(importance, size);

		expect(obj).toEqual(
			{
			"height" : "100px",
			"width" : "100px"
			});
	});

	it('Should return default size obj if input parameters are not numbers', function() {
		
		var obj1 = styleSizeByImportance('NaN');
		var obj2 = styleSizeByImportance('NaN', 100);
		var obj3 = styleSizeByImportance(50, 'NaN');
		var obj4 = styleSizeByImportance('NaN','NaN');

		expect(obj1).toEqual(defaultSizeObj);
		expect(obj2).toEqual(defaultSizeObj);
		expect(obj3).toEqual(defaultSizeObj);
		expect(obj4).toEqual(defaultSizeObj);
	});

	it('Should return input size if input importance is < 0,'
		+ ' default object if no size is specified',
		 function() {

			var importance = -1;
			var size = 100;

			var obj1 = styleSizeByImportance(importance);
			var obj2 = styleSizeByImportance(importance, size);
			
			var tObj1 = defaultSizeObj;

			var tObj2 = {
						 "height" : (size + "px"),
						 "width"  : (size + "px")
						};
			
			expect(obj1).toEqual(tObj1);
			expect(obj2).toEqual(tObj2);

	});

	it('Should return input size if input parameter *importance* is > 100,'
		+ ' default object if no size is specified',
		 function() {

			var importance = 101;
			var size = 100;

			var obj1 = styleSizeByImportance(importance);
			var obj2 = styleSizeByImportance(importance, size);
			
			var tObj1 = defaultSizeObj;

			var tObj2 = {
						 "height" : (size + "px"),
						 "width"  : (size + "px")
						};
			
			expect(obj1).toEqual(tObj1);
			expect(obj2).toEqual(tObj2);
	});

	it('Should return an obj that uses default size if input parameter' +
		' *size* is < 0', function() {
			var dSize = 64
			var size = -1;
			var importance1 = 50;
			var importance2 = 75;

			var obj1 = styleSizeByImportance(importance1, size);
			var obj2 = styleSizeByImportance(importance2, size);

			var size1 = (dSize * importance1) / 100;
			var size2 = (dSize * importance2) / 100;

			var tObj1 = {
						 "height" : (size1 + "px"),
						 "width"  : (size1 + "px")
						};
			var tObj2 = {
						 "height" : (size2 + "px"),
						 "width"  : (size2 + "px")
						};

			expect(obj1).toEqual(tObj1);
			expect(obj2).toEqual(tObj2);
	});

	it('Should return an obj whose height and width  are equal', function() {

		var obj1 = styleSizeByImportance();
		var obj2 = styleSizeByImportance(50);
		var obj3 = styleSizeByImportance(100,200);

		expect(obj1.height).toEqual(obj1.width);
		expect(obj2.height).toEqual(obj2.width);
		expect(obj3.height).toEqual(obj3.width);
	});

});


describe('styleRelPosition', function() {

	var defaultPosObj = {
				 "top" : "0%",
				 "left" : "50%"
				}

	it('Should return a default top & left *position* object', function() {
		var obj = styleRelPosition();

		expect(obj).toEqual(defaultPosObj);
	});


	it('Should position output as (x,y) coordinates of' +
	 ' input parameters *centrality* & *recency*, respectively', function() {
		
		/* set defaults */

		var centrality0 = -50;
		var centrality1 = 0;
		var centrality2 = 47;
		var centrality3 = 100;
		var centrality4 = 111;

		var recency0 = -25;
		var recency1 = 0;
		var recency2 = 33;
		var recency3 = 100;
		var recency4 = 104;

		var obj0 = styleRelPosition(centrality0, recency0);
		var obj1 = styleRelPosition(centrality1, recency1);
		var obj2 = styleRelPosition(centrality2, recency2);
		var obj3 = styleRelPosition(centrality3, recency3);
		var obj4 = styleRelPosition(centrality4, recency4);

		/* set up test objects */
		var tObj0 = {
					  "top"  : ((100 - recency0) + "%"),
					  "left" : (centrality0 + "%")
					};

		var tObj1 = {
					  "top"  : ((100 - recency1) + "%"),
					  "left" : (centrality1 + "%")
					};

		var tObj2 = {
					  "top"  : ((100 - recency2) + "%"),
					  "left" : (centrality2 + "%")
					};

		var tObj3 = {
					  "top"  : ((100 - recency3) + "%"),
					  "left" : (centrality3 + "%")
					};

		var tObj4 = {
					  "top"  : ((100 - recency4) + "%"),
					  "left" : (centrality4 + "%")
					};

		expect(obj0).toEqual(tObj0);
		expect(obj1).toEqual(tObj1);
		expect(obj2).toEqual(tObj2);
		expect(obj3).toEqual(tObj3);
		expect(obj4).toEqual(tObj4);
	});


	it('Should return default position obj if input parameters are not numbers', function() {
		
		var obj0 = styleRelPosition('NaN');
		var obj1 = styleRelPosition('NaN', 100);
		var obj2 = styleRelPosition(50, 'NaN');
		var obj3 = styleRelPosition('NaN','NaN');

		expect(obj0).toEqual(defaultPosObj);
		expect(obj1).toEqual(defaultPosObj);
		expect(obj2).toEqual(defaultPosObj);
		expect(obj3).toEqual(defaultPosObj);
	});

});



