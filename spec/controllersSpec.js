/* Jasmine Unit Testing - controllersSpec */
/* (Unit tests for all controllers in controllers.js) */

//require('./assets/js/dependencies/angular-mock.js');
///<reference path="Users/jabariholloway/projects/jabariholloway/helpers/angular-mock.js" />
//require(__dirname + '/../../helpers/angular-mock.js');
//require(__dirname + '/../../content/achievements.json');

var scope, ctrl, $httpBackend;

beforeEach(module('jhApp'));

describe('HomeCtrl', function() {
	
	beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('/home/achievements').
			respond([{"name" : "Formed UnTied Shoes musical group"}, 
					 {"name" : "Designed & Developed JabariHolloway.com"}]);
		$httpBackend.expectGET('/home/stories').
			respond([{"name" : "Rio Revelations"},
					{"name" : "UnTied Shoes: Restaurant Blues"}]);

		scope = $rootScope.$new();
		ctrl = $controller('HomeCtrl', {$scope: scope});
	}));

	it('Should create an achievements model with 2 achievements feteched from xhr', function() {
			expect(scope.achievements).toBeUndefined();
			$httpBackend.flush();

			expect(scope.achievements).toEqual([
				{"name" : "Formed UnTied Shoes musical group"}, 
				{"name" : "Designed & Developed JabariHolloway.com"}]);
	});

	it('Should create a stories model with 2 stories fetched from xhr', function() {
			expect(scope.stories).toBeUndefined();
			$httpBackend.flush();

			expect(scope.stories).toEqual([
						{"name" : "Rio Revelations"},
						{"name" : "UnTied Shoes: Restaurant Blues"}]);
	});

	it('Should assign a function to the "setSize" property', function() {
			$httpBackend.flush();
			expect(typeof scope.setSize).toBe('function');
	});

	it('Should assign a function to the "setPosition" property', function() {
			$httpBackend.flush();
			expect(typeof scope.setPosition).toBe('function');
	});

});


describe('AchievementsCtrl', function() {
	



	beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('/achievements').
			respond([{"name" : "Formed UnTied Shoes musical group"}, 
					 {"name" : "Designed & Developed JabariHolloway.com"}]);

		scope = $rootScope.$new();
		ctrl = $controller('AchievementsCtrl', {$scope: scope});
	}));

	it('Should create an achievements model with 2 achievements feteched from xhr', function() {
			expect(scope.achievements).toBeUndefined();
			$httpBackend.flush();

			expect(scope.achievements).toEqual([
				{"name" : "Formed UnTied Shoes musical group"}, 
				{"name" : "Designed & Developed JabariHolloway.com"}]);
	});

	it('Should assign a function to the "setSize" property', function() {
			$httpBackend.flush();
			expect(typeof scope.setSize).toBe('function');
	});

	it('Should assign a function to the "setPosition" property', function() {
			$httpBackend.flush();
			expect(typeof scope.setPosition).toBe('function');
	});

});

describe('StoriesCtrl', function() {


	beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
		$httpBackend = _$httpBackend_;
		$httpBackend.expectGET('/stories').
			respond([{"name" : "Rio Revelations"},
					{"name" : "UnTied Shoes: Restaurant Blues"}]);
		scope = $rootScope.$new();
		ctrl = $controller('StoriesCtrl', {$scope: scope});
	}));

	it('Should create a stories model with 2 stories fetched from xhr', function() {
			expect(scope.stories).toBeUndefined();
			$httpBackend.flush();

			expect(scope.stories).toEqual([
						{"name" : "Rio Revelations"},
						{"name" : "UnTied Shoes: Restaurant Blues"}]);
	});

	it('Should assign a function to the "setSize" property', function() {
			$httpBackend.flush();
			expect(typeof scope.setSize).toBe('function');
	});

	it('Should assign a function to the "setPosition" property', function() {
			$httpBackend.flush();
			expect(typeof scope.setPosition).toBe('function');
	});
});