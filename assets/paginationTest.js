var paginate3 = function(storyObj, pageContentHeight, pageContentWidth, linesPerPage) {

	//handle first page later
	//what should paragraph dimensions be on first page?


	//pages two and beyond
	//content comes in as an array of text elements than 
	var linesPerPage = linesPerPage || 48;
	//i should be able to get the number of lines in each content array elemement
	//but the number of lines is based on the page (content) width, which is 
	//720px currently
	var pgContentHeight = pageContentHeight || 900; //set page height to 900px;
	var pgContentWidth = 720;

	//now there is a 720x900 content box to fill
	//in order to create a line, i would have to know the 
	//maximum chars that would 

	function isOverflowed(element){
    	return element.scrollHeight > element.clientHeight || element.scrollWidth > element.clientWidth;
	}

	function createPage(storyInfo) {
		var page = document.createElement("div");
			page.className = "jh-story-page";
			var contentContainer = document.createElement("div");
				contentContainer.className = "jh-page-content-container";
				var storyText = document.createElement("p");
				storyText.className = "jh-story-text";

				if (storyInfo) {
					var title = document.createElement("h1");
						title.id = "jh-story-title";
					var author = document.createElement("h5");
						author.id = "jh-story-author";
						author.className = "jh-story-info";
					var pubDate = document.createElement("h6");
						pubDate.id = "jh-story-date"
						pubDate.className = "jh-story-info";

					storyText.className += " jh-story-p-first";

					//create text nodes for title, author, and date
					var titleVal = document.createTextNode(storyInfo.title);
					var authorVal = document.createTextNode(storyInfo.author);
					var pubDateVal = document.createTextNode(formatDate(storyInfo.date));

					//add text nodes to title, author, and date
					title.appendChild(titleVal);
					author.appendChild(authorVal);
					pubDate.appendChild(pubDateVal);

					//append title, author, and pubDate to page
					page.appendChild(title);
					page.appendChild(author);
					page.appendChild(pubDate);
				}

			//append elements
			contentContainer.appendChild(storyText);
			page.appendChild(contentContainer);

			return page;
	}

	//add content to the DOM

}


function createPage(storyInfo) {
		var page = document.createElement("div");
			page.className = "jh-story-page";
			var contentContainer = document.createElement("div");
				contentContainer.className = "jh-page-content-container";
				var storyText = document.createElement("p");
				storyText.className = "jh-story-text";

				if (storyInfo) {
					var title = document.createElement("h1");
						title.id = "jh-story-title";
					var author = document.createElement("h5");
						author.id = "jh-story-author";
						author.className = "jh-story-info";
					var pubDate = document.createElement("h6");
						pubDate.id = "jh-story-date"
						pubDate.className = "jh-story-info";

					storyText.className += " jh-story-p-first";

					//create text nodes for title, author, and date
					var titleVal = document.createTextNode(storyInfo.title);
					var authorVal = document.createTextNode(storyInfo.author);
					var pubDateVal = document.createTextNode(formatDate(storyInfo.date));

					//add text nodes to title, author, and date
					title.appendChild(titleVal);
					author.appendChild(authorVal);
					pubDate.appendChild(pubDateVal);

					//append title, author, and pubDate to page
					page.appendChild(title);
					page.appendChild(author);
					page.appendChild(pubDate);
				}

			//append elements
			contentContainer.appendChild(storyText);
			page.appendChild(contentContainer);

			return page;
	}