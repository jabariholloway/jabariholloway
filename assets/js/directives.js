/* Angular JS Directives */

//Retrieve JH App
var jhAppDRef = angular.module('jhApp');

//Retrieve Footer Controller
//var footerCtrl = angular.contoller('FooterCtrl');


jhAppDRef.directive('modalDialog', function() {
	return {
		restrict: 'E',
		replace: true,
		transclude: true,
		scope: {
				 modalId: '@',
				 hideModal: '&',
				 visible: '=',
				 feedbackMode: '=',
				 hideFeedback: '&',
				 resetFormIfFeedback: '&',
				 nestedForm: '='
			   },
		link: function (scope, element) {
			var form = element.find('form');
			scope.formCtrl = form.controller('form');

			scope.$on('$destroy', function() {
				form = null;
				scope.formCtrl = null;
				this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
				this.$$childTail = null;
			});
		},
		template: '<div ng-show="visible">' +
					  '<div class="jh-modal-overlay" ng-click="hideModal(); resetFormIfFeedback({form : formCtrl});  hideFeedback();"></div>' +
					  '<div class="jh-modal-burst" ng-click="hideModal(); resetFormIfFeedback({form: formCtrl}); hideFeedback();"></div>' +
					  '<div id="{{ modalId }}" ng-show="!feedbackMode" class="jh-modal-dialog container jh-container"> ' +
					  '  <div id="{{modal}}" class="row-fluid jh-modal-close-row"> ' +
					  '  		<div class="col-xs-12 jh-modal-close-col">' + 
					  '			<div class="jh-x3 pull-right" ng-click="hideModal()">&#9587;</div>' +
					  '		</div>' +
					  '  </div>' +
					  '  <div class="row-fluid"> ' +
					  '      <div class="col-xs-12">' +
					  '			<div class="jh-modal-dialog-content" ng-transclude></div>' +
					  '		</div>' +
					  '	</div>' +
				  	  '</div>'
	}
});

jhAppDRef.directive('contentBackground', function() {
	return {
		restrict: 'E',
		replace: true,
		transclude: true,
		scope: {
				click: '&',
				modalMode: '='
			   },
		link: function(scope) {
			scope.$on('$destroy', function() {
				this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
				this.$$childTail = null;
			});
		},
		template: '<div>' +
					  '<div class="jh-modal-overlay" ng-click="click()"></div>' +
					  '<div class="jh-content-burst jh-tsn-content-burst-before" ng-class="{' + "'jh-hide'" + ': modalMode === true }" ></div>' +
					  '<div class="jh-content-container-outer" ng-class="{' + "'jh-slightly-transparent'" + ': modalMode === true }" ng-transclude></div>' +
				   '</div>'
	}
});

jhAppDRef.directive('contentClose', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			url: '@'
		},
		link: function(scope) {
				scope.$on('$destroy', function() {
					this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
					this.$$childTail = null;
				});
		},
		template: 	'<div class="jh-content-close container jh-container"> ' +
					'	<div class="row-fluid jh-modal-close-row"> ' +
					'		<div class="col-xs-12 jh-modal-close-col">' +
					'			<a ng-href="{{url}}" target="_self" class="jh-x3 pull-right">&#9587;</a>' +
					'		</div>' +
					'	</div>' +
					'</div>' 
	}
});








