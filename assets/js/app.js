/*$(document).ready(function(){

	/*$('.jh-record').hover(
		function() {
			$(this).find('.jh-star').css('background', 'radial-gradient(circle, rgba(255, 255, 255,1), rgba(0, 0, 150, 0.5), rgba(255, 255, 255, 0.1), rgba(0, 0, 0, 0.8))')
			$(this).find('.jh-label').css('color', '#2badd4')
		},
		function() {
			$(this).find('.jh-star').css('background', 'radial-gradient(circle, rgba(255, 255, 255,1), rgba(0, 0, 200, 0.3), rgba(0, 0, 0, 0))')
			$(this).find('.jh-label').css('color', '#ffffff')
		})
})*/

var jhApp = angular.module('jhApp',['ngRoute', 'ui.bootstrap', 'ngSanitize']);

/* importance should be a number from 0 - 100 */
var styleSizeByImportance = function(importance, size) {
	/* default */
	var dSize = 64; //default size is 64x64px
	var dImportance = 100; //defaults to most important

	var _size = dSize;
	var _importance = dImportance;

	if (size && !isNaN(size) && size >= 0) {
		_size = size;
	}

	if (importance && !isNaN(importance) && importance > 0 && importance <= 100) {
		_importance = importance;
	}

	if (importance === 0) {
		_importance = 0;
	}


	if ((importance && size) && (isNaN(size) || isNaN(importance))) {
		_size = dSize;
		_importance = dImportance;
	}

	_size = (_size * _importance) / 100; //parentheses for clarity, not correctness
	_size = _size + "px";

	return {
			"height" : _size,
			"width" : _size
			};
}

/* y should be a number from 0 - 100 */
/* x should be a number from 0 - 100 */

var styleRelPosition = function(x, y) {
	/* defaults  (top center) */
	var dfltX = 50; //max x (midpoint)
	var dfltY = 100; //most recent

	var _x = dfltX;
	var _y = dfltY;

	if (x && !isNaN(x)) {
		_x = x;
	}

	if (y && !isNaN(y)) {
		_y = y;
	}

	if (x === 0) {
		_x = x;
	}

	if (y === 0) {
		_y = y;
	}

	if (isNaN(x) || isNaN(y)) {
		_x = dfltX;
		_y = dfltY;
	}

	/* adjust for browser coordinate system reference point */
	_y = (100 - _y) + "%"
	_x = _x + "%"

	return {
			"top" : _y,
			"left" : _x
		   };
}

/*Accepts a date String of format MM/DD/YYYY
*Returns a format of "Mon, Dth(st,nd,rd),YYYY"
*/
/*use moment.js in the future */
var formatDate = function(date) {
	//check if date is string and matches MM/DD/YYYY
	//if not, return null or throw error
	if (typeof date === "string") {
		if (/^\d\d\/\d\d\/\d\d\d\d$/.test(date)) {
			var sArray = date.split('/');

			var month = "";

			switch(sArray[0]) {
				case "01" :
				  month = "Jan";
				  break;
				case "02" :
				  month = "Feb";
				  break;
				case "03" :
				  month = "Mar";
				  break;
				case "04" :
				  month = "Apr";
				  break;
				case "05" :
				  month = "May";
				  break;
				case "06" :
				  month = "Jun";
				  break;
				case "07" :
				  month = "Jul";
				  break;
				case "08" : 
				  month = "Aug";
				  break;
				case "09" :
				  month = "Sep";
				  break;
				case "10" :
				  month = "Oct";
				  break;
				case "11" :
				  month = "Nov";
				  break;
				case "12" :
				  month = "Dec";
				  break;
				default :
				  month = "";
				  break;
			}

			var day = parseInt(sArray[1]);


			if ((day % 10) >= 0 && (day % 10) <= 3 && day != 12 && day != 13 && day != 14) {
				switch(day % 10) {
				case 0 :
				  day = day + "th";
				  break;
				case 1 :
				  day = day + "st";
				  break;
				case 2 :
				  day = day + "nd";
				  break;
				case 3 :
				  day = day + "rd";
				  break;
				default :
				  day = "";
				  break;
				}
			} else if (day > 3 && day <= 9) {
				day = sArray[1].substring(1,2) + "th";
			} else if (day > 9) {
				day = sArray[1] + "th";
			}
			

			if (day.substring(0, day.length - 2) === '11') {
				day = day.replace('st', 'th');
			}

			var year = sArray[2];

			return month + " " + day + ", " + year;
		}
	}
	return "";
}

var isURL = function(str) {
	return /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i.test(str);
}

var isRoute = function(str) {
	return /^(\/[\da-z\.-]+)+$/i.test(str);
}

var range = function(min, max, step) {
    step = step || 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
        input.push(i);
    }
    return input;
}

/*write function to get numPages*/

/*Function: reformats inputs into an array of content 
*where each element represents the entire content that fits 
*on a single page.
*Input: string array
*Output: string array
*/

//needs to fix the max number of lines, not the max number of characters (too many goofy line breaks)
var paginate2 = function(content, numPages, titleHeight, pageHeight, vertMargins, fontSize, charsPerLine) {
	var pages = [];
	if (!content) {
		return pages;
	}

	//account (more) for margins and also account for line breaks

	var titleHt = titleHeight || 0; //px
	var pgHt = pageHeight || 1056; //px
	var vMrgns = vertMargins || 300; //px
	var fSz = fontSize || 14; //px
	var chrsPerLn = charsPerLine || 100;

	var maxPgHt = pgHt - vMrgns - 200;
	var maxLnsPerPg = maxPgHt / fSz; //46 is about the best #
	var maxChrsPerPg = maxLnsPerPg * chrsPerLn;
	console.log("max chars per page " + maxChrsPerPg);

	var i = 0;

	//content index
	var cIndx = 0;


	//format first page
	if (titleHt != 0) {
		console.log('title height greater than 0');
		var fMaxPgHt = maxPgHt - titleHt;
		var fMaxLnsPerPg = fMaxPgHt / fSz;
		var fMaxChrsPerPg = fMaxLnsPerPg * chrsPerLn;
		console.log(fMaxChrsPerPg);
		var page = "";
		while (page.length < fMaxChrsPerPg) {

			if (cIndx >= content.length) {
				break;
			}

			var paragraph = content[cIndx];
			var tPage = page + paragraph;

			if (tPage.length >= fMaxChrsPerPg) {
				//break the content where it reaches the character limit
				//add the first half to the current page
				//prepend the second half to the next paragraph elem, if such an element exits
				var delta = paragraph.length + page.length - fMaxChrsPerPg;

				//need to find the nearest word or token to the split index to split on
				var splitIndex = findNearestWordBreakIndex(paragraph, paragraph.length - delta);
				var firstHalf = paragraph.substring(0, splitIndex);
				var secondHalf = paragraph.substring(splitIndex, paragraph.length);
				page += firstHalf;
				if (content[cIndx + 1]) {
					content[cIndx + 1] = secondHalf + content[cIndx + 1];
					cIndx++;
				}
				break;
			}

			page += paragraph + "<br><br>";
			cIndx++; 
		}
		pages.push(page);
		i = 1;
	} 



	for (i; i < numPages + 1; i++) {
		var page = "";
		while (page.length < maxChrsPerPg) {

			if (cIndx >= content.length) {
				break;
			}

			var paragraph = content[cIndx];
			var tPage = page + paragraph;

			if (tPage.length >= maxChrsPerPg) {
				//break the content where it reaches the character limit
				//add the first half to the current page
				//prepend the second half to the next paragraph elem, if such an element exits
				var delta = paragraph.length + page.length - maxChrsPerPg;

				//need to find the nearest word or token to the split index to split on
				var splitIndex = findNearestWordBreakIndex(paragraph, paragraph.length - delta);
				var firstHalf = paragraph.substring(0, splitIndex);
				var secondHalf = paragraph.substring(splitIndex, paragraph.length);
				page += firstHalf;
				if (content[cIndx + 1]) {
					content[cIndx + 1] = secondHalf + content[cIndx + 1];
					cIndx++;
				}
				else {
					//we've reached the last index 
					//so assign the secondHalf to the current cIndx elem
					content[cIndx] = secondHalf;
				}
				break;
			}

			page += paragraph + "<br><br>";
			cIndx++; 
		}
		pages.push(page);
	}

	return pages;
}

var paginate = function(content, numPages, titleHeight, pageHeight, vertMargins, fontSize, charsPerLine) {
	var pages = [];
	if (!content) {
		return pages;
	}

	//account (more) for margins and also account for line breaks

	var titleHt = titleHeight || 0; //px
	var pgHt = pageHeight || 1056; //px
	var vMrgns = vertMargins || 300; //px
	var fSz = fontSize || 14; //px
	var chrsPerLn = charsPerLine || 100;

	var maxPgHt = pgHt - vMrgns - 200;
	var maxLnsPerPg =  maxPgHt / fSz; //46 is about the best #
	var maxChrsPerPg = maxLnsPerPg * chrsPerLn;
	//console.log("max chars per page " + maxChrsPerPg);

	var i = 0;

	//content index
	var cIndx = 0;


	//format first page
	if (titleHt != 0) {
		//console.log('title height greater than 0');
		var fMaxPgHt = maxPgHt - titleHt;
		var fMaxLnsPerPg = fMaxPgHt / fSz;
		var fMaxChrsPerPg = fMaxLnsPerPg * chrsPerLn;
		//console.log(fMaxChrsPerPg);
		var page = "";
		while (page.length < fMaxChrsPerPg) {

			if (cIndx >= content.length) {
				break;
			}

			var paragraph = content[cIndx];
			var tPage = page + paragraph;

			if (tPage.length >= fMaxChrsPerPg) {
				//break the content where it reaches the character limit
				//add the first half to the current page
				//prepend the second half to the next paragraph elem, if such an element exits
				var delta = paragraph.length + page.length - fMaxChrsPerPg;

				//need to find the nearest word or token to the split index to split on
				var splitIndex = findNearestWordBreakIndex(paragraph, paragraph.length - delta);
				var firstHalf = paragraph.substring(0, splitIndex);
				var secondHalf = paragraph.substring(splitIndex, paragraph.length);
				page += firstHalf;
				if (content[cIndx + 1]) {
					content[cIndx + 1] = secondHalf + content[cIndx + 1];
					cIndx++;
				}
				break;
			}

			page += paragraph + "<br><br>";
			cIndx++; 
		}
		pages.push(page);
		i = 1;
	} 



	for (i; i < numPages + 1; i++) {
		var page = "";

		while (page.length < maxChrsPerPg) {

			if (cIndx >= content.length) {
				break;
			}

			var paragraph = content[cIndx];
			var tPage = page + paragraph;
			lineCount(page);
			lineCount(tPage);

			if (lineCount(tPage) >= maxLnsPerPg) {
				//break the content where it reaches the character limit
				//add the first half to the current page
				//prepend the second half to the next paragraph elem, if such an element exits
				var delta2 = Math.floor(lineCount(tPage) - maxLnsPerPg) - 2; //lines (subtracting 2 removes error in maxLnsPerPg)
				//console.log("delta-" + i + " in lines: " + delta2);

				//need to find the right line split (i.e. where the excess lines begin)
				//need to find the nearest word or token to the split index to split on
				//--var splitIndex = findNearestLineBreakIndex(paragraph, delta, charsPerLine);
				var delta = paragraph.length + page.length - maxChrsPerPg;
				//console.log('delta in chars reg. calc: ' + delta);
				delta2 *= chrsPerLn; //chars
				var splitIndex = findNearestWordBreakIndex(paragraph, paragraph.length - delta2 + 40);

	
				//console.log('delta in chars lines calc: ' + delta2);
				var firstHalf = paragraph.substring(0, splitIndex);
				var secondHalf = paragraph.substring(splitIndex, paragraph.length);
				page += firstHalf;
				if (content[cIndx + 1]) {
					content[cIndx + 1] = secondHalf + content[cIndx + 1];
					cIndx++;
				}
				else {
					//we've reached the last index 
					//so assign the secondHalf to the current cIndx elem
					content[cIndx] = secondHalf;
				}
				break;
			}

			page += paragraph + "<br><br>";
			cIndx++; 
		}
		pages.push(page);
	}

	return pages;
}

var findNearestLineBreakIndex = function(paragraph, delta, charsPerLine) {
	//the delta is how many lines i need to shave off of page + paragraph
	//but, i only calculated the delta because the line count of page + paragraph
	//is greater than the line limit.
	//so, the delta actually tells me how many "lines" i need to cut from the paragraph

	//(lineCount(paragraph) - delta) will tell me at what point, starting from the beginning
	//of the paragraph where i need to make the split

	//once i know after how many lines i need to make the split, i can multiply that 
	//value by charsPerLine to get an index. then i can use findNearestWordBreakIndex
	//to return the index i need to substring on
	var lines = lineCount(paragraph) - delta;
	//console.log('findNearestLineBreakIndex lines ' + lines);
	var chars = lines * charsPerLine;
	return findNearestWordBreakIndex(paragraph, chars);

}




//

var findNearestWordBreakIndex = function(paragraph, index) {
	var wordBreakIndex = index;
	var char = paragraph.charAt(wordBreakIndex);

	while (char != ' ' && char != '.') {
		wordBreakIndex--;
		char = paragraph.charAt(wordBreakIndex);
		if (wordBreakIndex < 0) {
			break;
		}
	}

	return wordBreakIndex;
}


var lineCount = function(page, charsPerLine) {
	var chrsPerLn = charsPerLine || 100;

	var lineCount = Math.ceil(page.length / 100);

	//console.log('initial line count ' + lineCount);

	lineCount += Math.ceil((page.match(/<br>/g) || []).length);

	//console.log('final line count ' + lineCount);

	return lineCount;
}

/*credit: russelluresti */
function findKeyframesRule(rule)
    {
        // gather all stylesheets into an array
        var ss = document.styleSheets;
        
        // loop through the stylesheets
        for (var i = 0; i < ss.length; ++i) {
            
            // loop through all the rules
            for (var j = 0; j < ss[i].cssRules.length; ++j) {
                
                // find the -webkit-keyframe rule whose name matches our passed over parameter and return that rule
                if (ss[i].cssRules[j].type == window.CSSRule.WEBKIT_KEYFRAMES_RULE && ss[i].cssRules[j].name == rule)
                    return ss[i].cssRules[j];
            }
        }
        
        // rule not found
        return null;
    }

/*credit: russelluresti */
function changeAnimation(anim, oldRule, newRule, elemClass)
    {
        // find our -webkit-keyframe rule
        var keyframes = findKeyframesRule(anim);
        
        // remove the existing from rule
        keyframes.deleteRule(oldRule);
        
        // create new rule
        keyframes.appendRule(newRule);

        var elem = document.getElementsByClassName(elemClass)[0];

        //remove current animation
        /*elem.style.webkitAnimationName = "none";
        
        // assign the animation to our element (which will cause the animation to run)
        elem.style.webkitAnimationName = anim;*/
    }

/*credit: david walsh & underscore.js */
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

function getRawFloatValue(cssProp) {
	if (typeof cssProp === 'string') {
		if (cssProp.substring(cssProp.length - 1, cssProp.length) === '%') {
			return parseFloat(cssProp.substring(0, cssProp.length - 1));
		}
		else if (cssProp.substring(cssProp.length - 2, cssProp.length) === 'px') {
			 return parseFloat(cssProp.substring(0, cssProp.length - 2));
		} else {
			return parseFloat(cssProp);
		}
	} else {
		return cssProp;
	}
}

//Angular & scope cleanup function to mitigate memory leaks
window.onbeforeunload = function(e) {
	if(angular) {
		var scopes = document.getElementsByClassName('ng-scope');
		for (var i = scopes.length - 1; i >= 0; i--) {
				var ngEl = angular.element(scopes[i]);
				var scp = ngEl.scope();
				scp.$destroy();
		}

		var isolateScopes = document.getElementsByClassName('ng-isolate-scope');
		for (var i = isolateScopes.length - 1; i >= 0; i--) {
				var ngEl = angular.element(isolateScopes[i]);
				var iscp = ngEl.scope();
				iscp.$destroy();
		}

		jhApp = null;
		if (jhAppCRef) {
			jhAppCRef = null;
		}
		if (jhAppSRef) {
			jhAppSRef = null;
		}
		if (jhAppDRef) {
			jhAppDRef = null;
		}
		angular = null;
	}
	if($) {
		JQuery.cache.length = 1;
		$ = null;
	}
}

var getIterArray = function(maxVal) {
	var a = [];

	for (var i = 0; i < maxVal; i++) {
		a.push(i);
	}
	return a;
}

var objCount = function(obj) {
	var key, count = 0;
	for(key in obj) {
  		if(obj.hasOwnProperty(key)) {
    	count++;
  	}
  }
  return count;
}

//Handle mobile keyboard pop-up

//Get and store initial screen height
var initialScreenHeight;
var mobileKeyboardResize;

$(document).ready(function(){

if (document.documentElement.clientWidth <= 767) {

	initialScreenHeight = document.documentElement.clientHeight;

	//set window resize event handler 
	mobileKeyboardResize = function() {
		//get body element
		document.getElementsByTagName("body")[0].style.height = initialScreenHeight + "px"; 
		document.getElementById("jh-footer").style.position = "absolute";
		var modals = document.getElementsByClassName("jh-modal-dialog");
			for (var i = 0; i < modals.length; i++) {
				modals[i].style.position = "absolute";
			}
		var bursts = document.getElementsByClassName("jh-modal-burst");
			for (var i = 0; i < bursts.length; i++) {
				bursts[i].style.position = "absolute";
			}
		var overlays = document.getElementsByClassName("jh-modal-overlay");
			for (var i = 0; i < overlays.length; i++) {
				overlays[i].style.position = "absolute";
			}
		document.getElementById("jh-photo-gradient").style.position = "absolute";
		document.getElementById("jh-photo").style.marginTop = "0";
	}

	mobileKeyboardResize();
}


});

