/* Angular JS controllers */

var jhAppCRef = angular.module('jhApp');

/*Routes*/
jhAppCRef.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
	$routeProvider.
	when('/achievements/:name', {
		controller: 'AchievementsCtrl'
	}).
	when('/stories/:name', {
		controller: 'StoriesCtrl'
	}).
	when('/#achievements', {
		controller: 'HomeCtrl'
	}).when('/#stories', {
		controller: 'HomeCtrl'
	}).when('/#about', {
		controller: 'FooterCtrl'
	}).when('/#subscribe', {
		controller: 'FooterCtrl'
	}).when('/#contact', {
		controller: 'FooterCtrl'
	});

	$locationProvider.html5Mode(true).hashPrefix('~');
}]);

jhApp.controller('HomeCtrl', ['$scope', '$http', 'Modal', function($scope, $http, $modal) {
 
		/*function HomeCtrlTag() {}

		$scope.__tag = new HomeCtrlTag();*/

		$http.get('/home/achievements').success(function(data) {
			$scope.achievements = data;
		});

			$http.get('/home/stories').success(function(data) {
			$scope.stories = data;
		});

			$scope.setSize = styleSizeByImportance;
			$scope.setPosition = styleRelPosition;
			$scope.showAchievs;
			$scope.showStories;
			$scope.modal = $modal;

			$scope.setLocHash = function(val) {
				//console.log(typeof val);
				if (typeof val === 'string') {
				window.location.hash = val;
				}
			}


			

			//no length check needed. [simplified] slice takes slice(0,0) in the case of an empty string
			var h = location.hash.slice(1);


			if (h === 'achievements') {
				$scope.showAchieves = true; 
				$scope.showStories = false;
			} else if (h === 'stories') {
				$scope.showStories = true; 
				$scope.showAchieves = false;
			} else {
				$scope.setLocHash('#about');
			}

		$scope.$on('destroy', function(){
			$scope.achievements = null;
			$scope.stories = null;
			$scope.setSize = null;
			$scope.setPosition = null;
			$scope.showAchievs = null;
			$scope.showStories = null;
			$scope.modal = null;
			$scope.showAchieves = null; 
			$scope.showStories = null;
			h = null;
			this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
			this.$$childTail = null;
		});
	
}]);

jhAppCRef.controller('FooterCtrl', ['$scope', 'Modal', '$http', function($scope, $modal, $http) {
	$scope.modal = $modal;
	$scope.subscribeForm = {};
	$scope.contactForm = {};
	$scope.formFeedback = "";
	$scope.showFeedback = false;
	$scope.currentForm = {};
	$scope.loading = false;

	location.pathname === '/' && location.hash === '#about' ? $scope.hasEntered = false : $scope.hasEntered = true;


	console.log(location.hash);
	console.log(location.pathname);
	console.log($scope.hasEntered);

	//Get csrf token
	/*$http.get('/csrfToken').success(function(data) {
		$scope.subscribeForm._csrf = data._csrf;
		$scope.contactForm._csrf = data._csrf;
	});*/

	/*$scope.hideModal = function() {
		$scope.modal = false;
	}*/

	$scope.toggleAbout = function() {
		$scope.showContact = false;
		$scope.showSubscribe = false;
	    $scope.showAbout = !$scope.showAbout;
	    setModal();
	}

	$scope.toggleContact = function() {
		$scope.showAbout = false;
		$scope.showSubscribe = false;
	    $scope.showContact = !$scope.showContact;
		setModal();
	}

	$scope.toggleSubscribe = function() {
		$scope.showAbout = false;
		$scope.showContact = false;
	    $scope.showSubscribe = !$scope.showSubscribe;
		setModal();
	}

	$scope.removeFeedback = function() {
		$scope.showFeedback = false;
		$scope.formFeedback = "";
	}

	$scope.hideAllModals = function() {
		$scope.showAbout = false;
		$scope.showContact = false;
	    $scope.showSubscribe = false;
		setModal();
	}

	$scope.enter = function() {
		$scope.toggleAbout();
		$scope.hasEntered = true;
	}

	function setModal() {
		$modal.visible = $scope.showContact || $scope.showAbout || $scope.showSubscribe;
	}

	//check if form fields are valid - if so, return true, else return false
	//can set the validity of each form field to false if 
	function validate(form) {
		//iterate over object and check if the length of each field is > 0
		// also, if the field is an email, check that it's a valid email
		alert(JSON.stringify(form));
		for (var field in form) {
			if (form.hasOwnProperty(field)) {
				alert(field);
				alert(form[field].$modelValue);
			}
		}

	}

	$scope.resetFormIfFeedback = function(form) {
		if ($scope.showFeedback) {
			form.$setPristine();
			form.$setUntouched();
		}
	}

	$scope.subscribe = function(form) {
				if (form.$invalid) {
			//get the actual input elements 
			//for the ones that are invalid, 
			//set invalidity on them

			for (var field in form) {
				if (form.hasOwnProperty(field)) {
					//alert (field);
					//alert (typeof form[field]);
					if (typeof form[field] === 'object' && 
						typeof form[field].$invalid != "undefined" &&
						typeof form[field].$invalid) {
						
							form[field].$setTouched();
						
					}
				}
			}
			return;
		}

		$scope.showFeedback = true;
		$scope.loading = true;
		$scope.currentForm = form;

	//Get csrf token
	$http.get('/csrfToken').success(function(data) {
		$scope.subscribeForm._csrf = data._csrf;

		$http({
			method: 'POST',
			url : '/subscribe',
			data : $.param($scope.subscribeForm),
			headers : { 'Content-Type': 'application/x-www-form-urlencoded'},
			withCredentials: true
		}).then(function(res) {
			//alert(res.status);
			//alert(res.data);
			//alert('You have been subscribed!');
			$scope.formFeedback = "You have been subscribed!";
			$scope.loading = false;
			$scope.subscribeForm = {};
			//hide form, show form feedback
			//close modal, reset form values, and show new modal confirmation/error
			//$scope.toggleSubscribe();
		}, function(res) {
			//alert(res.status);
			//alert(res.data || "Request failed");
			$scope.formFeedback = "Oops, something went wrong...";
			$scope.loading = false;
			$scope.subscribeForm = {};
		});

	  });

	}

	$scope.contact = function(form) {
		if (form.$invalid) {
			//get the actual input elements 
			//for the ones that are invalid, 
			//set invalidity on them

			for (var field in form) {
				if (form.hasOwnProperty(field)) {
					//alert (field);
					//alert (typeof form[field]);
					if (typeof form[field] === 'object' && 
						typeof form[field].$invalid != "undefined" &&
						typeof form[field].$invalid) {
						
							form[field].$setTouched();
						
					}
				}
			}
			return;
		}

		$scope.showFeedback = true;
		$scope.loading = true;
		$scope.currentForm = form;

		//Get csrf token
	$http.get('/csrfToken').success(function(data) {
		$scope.contactForm._csrf = data._csrf;

		$http({
			method: 'POST',
			url : '/contact',
			data : $.param($scope.contactForm),
			headers : { 'Content-Type': 'application/x-www-form-urlencoded'},
			withCredentials : true
		}).then(function(res) {
			//alert(res.status);
			//alert(res.data);

			//need to check the response message to determine if message was sent
			if (res.data === 'success') {
				//alert('Message Sent!');
				$scope.formFeedback = "Message Sent!"
				$scope.loading = false;

				$scope.contactForm = {};
			}
			//close modal, reset form values, and show new modal confirmation/error
			//$scope.toggleContact();
		}, function(res) {
			//alert(res.status);
			//alert(res.data || "Request failed");
			$scope.formFeedback = "Oops, something went wrong...";
			$scope.loading = false;
			$scope.contactForm = {};
		});
	  });
	}

	//no length check needed. [simplified] slice takes slice(0,0) in the case of an empty string
		var h = location.hash.slice(1);
		if (h === 'about') {
			$scope.toggleAbout();
		} else if (h === 'subscribe') {
			$scope.toggleSubscribe();
		} else if (h === 'contact') {
			$scope.toggleContact();
		}

		$scope.$on('$destroy', function() {
			$scope.modal = null;
			$scope.subscribeForm = null;
			$scope.contactForm = null;
			$scope.formFeedback = null;
			$scope.showFeedback = null;
			$scope.currentForm = null;
			$scope.loading = null;
			$scope.showContact = null;
			$scope.showSubscribe = null;
	    	$scope.showAbout = null;
	    	$scope.contactForm._csrf = null;
	    	$scope.subscribeForm._csrf = null;
	    	h = null;
	    	this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
			this.$$childTail = null;
		});

}]);


/*The Acheivements Controller should fetch a single achievement record
 *by calling /Achievements/:Name based upon input parameter :Name*/
jhApp.controller('AchievementsCtrl', ['$scope', '$http', '$routeParams', 'Modal', '$interval', function($scope, $http, $routeParams, $modal, $interval) {
	
		/*function AchievesCtrlTag() {}

		$scope.__tag = new AchievesCtrlTag();*/

		$scope.modal = $modal;

		/* slide carousel config */
		$scope.myInterval = 0;
		$scope.noWrapSlides = false;
		$scope.active = 0;
		var slides = $scope.slides = [];
	    $scope.hasImgLink = false;
	    $scope.carouselVisible = false;
	    $scope.showCarousel = function() { $scope.carouselVisible = true; }
	    $scope.hideCarousel = function() { $scope.carouselVisible = false; }

		var currIndex = 0;

		$scope.achievementName = $routeParams.name;
		$scope.routeParams = $routeParams;

		var achvName = $routeParams.name;

		$scope.formatDate = formatDate;
		$scope.isString = angular.isString;
		$scope.isObject = angular.isObject;
		$scope.isURL = isURL;
		$scope.isRoute = isRoute;
		$scope.getIterArray = getIterArray;

		$scope.$on('$routeChangeSuccess', function() {

	    	//console.log($routeParams.name);
	    	$http.get('/achievements/find/' + $routeParams.name).success(function(data) {

	    		var achv =	$scope.achievement = data;
	    		$scope.achvDate = formatDate(achv.date);

	    		if (document.documentElement.clientWidth > 768) {

					var size = styleSizeByImportance(achv.importance);
					var pos = styleRelPosition(achv.centrality, achv.recency); 
					var newTop = (getRawFloatValue(pos.top) * 0.704659 + 9.68637 + 2.18) +  '%';
					var newLeft = (getRawFloatValue(pos.left) * 0.421694 + 10.2939 + 1.37) + '%'; //1.17
					//newTop = 406 + 15 + 'px';
					//newLeft = 509 + 15 + 'px';
					//need to correct for absolute positioning that is applied to burst
					//when the page is loaded
					//the issue is that the achv positioning applied on the home page
					//is relative to a container that starts at a certain x,y that 
					//is not (0,0).
					//on top of that, the percentage is relative to the height & width
					//of the container
					//so, knowing the starting (x,y) of the container
					//and its size relative to the the full page 
					//will allow me to convert the top and left percentages appropriately

					var newRule = 'from { width: ' + size.width + '; height: ' + size.height + 
					'; top: ' + newTop + '; left: ' + newLeft + '; }';
					changeAnimation('jh-content-burst-grow', 'from', newRule, '.jh-content-burst');
					var burst = document.getElementsByClassName('jh-content-burst')[0];
			}


				if (achv.details.personalAchievements && achv.details.personalAchievements.info[0].imgLink) {
						//console.log(JSON.stringify(achv.details.personalAchievements.info[0].imgURLs));
						$scope.hasImgLink = true;
					achv.details.personalAchievements.info[0].imgURLs.forEach(function(url, i) {
						slides.push({
							image: url,
							id: i
						});
						//console.log('added slides');
						//console.log(JSON.stringify(slides));
					});
				  
				}
			});
	 	});

	 	$scope.$on('$destroy', function() {
			$scope.modal = null;
			$scope.myInterval = null;
			$scope.noWrapSlides = null;
			$scope.active = null;
			slides = $scope.slides = null;
	    	$scope.hasImgLink = null;
	    	$scope.carouselVisible = null;
	    	$scope.achievementName = null;
			$scope.routeParams = null;
			$scope.formatDate = null;
			$scope.isString = null;
			$scope.isObject = null;
			$scope.isURL = null;
			$scope.isRoute = null;
			$scope.showCarousel = null;
	    	$scope.hideCarousel = null;
			$scope.getIterArray = null;
			$scope.achievement = null;
			this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
			this.$$childTail = null;
		});

		

	
}]);

jhApp.controller('StoriesCtrl', ['$scope', '$http', '$routeParams', '$window', 'Modal', function($scope, $http, $routeParams, $window, $modal) {
	
	$scope.formatDate = formatDate;
	$scope.debounce = debounce;
	$scope.range = range;
	$scope.modal = $modal;
	var footer = angular.element(document.querySelector('#jh-footer'));

	//function definitions
	var scrollFn = debounce(function() {
		    var windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
		    var body = document.body, html = document.documentElement;
		    var docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight,  html.scrollHeight, html.offsetHeight);
		    windowBottom = windowHeight + window.pageYOffset;
		    if (windowBottom >= docHeight - 50) {
		       //removeFootrTransition();
		       $scope.showFooter();
		    }
		    else {
		    	//addFootrTransition();
		    	$scope.hideFooter();
		    }
		}, 1000);

	var ftrMouseEntr = function() {
			footer.addClass('jh-footer-story-visible');
		}

	var ftrMouseLeave = function() {
			if (!$modal.visible) {
				footer.removeClass('jh-footer-story-visible');
			}
		}
	

	$scope.$on('$routeChangeSuccess', function() {
		var stryName = $routeParams.name;
		footer.addClass('jh-footer-story');

		$scope.showFooter = function() {
			footer.addClass('jh-footer-story-visible');
		}

		$scope.hideFooter = function() {
			if (!$modal.visible) {
				footer.removeClass('jh-footer-story-visible');
			}
		}

		footer.on('mouseenter', ftrMouseEntr);

		footer.on('mouseleave', ftrMouseLeave);

		angular.element($window).on("scroll", scrollFn);


		/*$http.get('/stories/info/' + stryName).success(function(data) {
			$scope.storyInfo = data;

			
			/*the json endpoint should return the actual story content, which should be kept in a separate file from
			*stories.json.
			*
			*keep track of the url to each specific story within the stories.json (storyurl)
			*

		});*/

			/* Using second http GET because multiline strings are not supported by JSON,
			* making it easier to manage content in separate files
			*/

		//ANGULAR has a bug in it's $http JSON Parse function
		//which returns a malFormed nested JSON array  in my case.
		//Perhaps the length of my nested JSON array is too long?
		//using JQuery here because it works.




		$.ajax({
			url : '/stories/find/' + stryName,
			type : 'GET',
			success : function(storyObj) {
			 //$scope.pgsLoaded = false;
			 //console.log($scope.pgsLoaded);

			 //only paginate if screen width is in the 900px range
			 //for mobile especially, don't paginate, just show the text
			 //in one large view (i.e. ng-repeat each paragraph and add
			 //<br> tags appropriately)	
			 paginate3(storyObj, '/#stories', function(ready) {
			 	if (ready) {
			 		$scope.pgsLoaded = true;
			 	}
			 });
			},
			error : function(jQXhr, status, error) {
				//handle later
			}
		});


	}); //here
	
	//teardown
	$scope.$on('$destroy', function() {
		angular.element($window).off("scroll", scrollFn);
		footer.off('mouseenter', ftrMouseEntr);
		footer.off('mouseleave', ftrMouseLeave);
		$scope.formatDate = null;
		$scope.getPagesArray = null;
		$scope.range = null;
		$scope.modal = null;
		footer = null;
		$scope.numPages = null;
		$scope.pages = null;
		$scope.storyInfo = null;
		$scope.storyContent = null;
		this.$parent = this.$$nextSibling = this.$$prevSibling = this.$$childHead =
		this.$$childTail = null;

	});


}]);








