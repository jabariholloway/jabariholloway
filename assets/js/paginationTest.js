
//return true as a way to know when all of the content has been loaded
//into the DOM for transition purposes
var paginate3 = function(storyObj, returnUrl, callback) {

	//handle first page later
	//what should paragraph dimensions be on first page?
	var storyCont = document.getElementById('jh-story-container');
	//console.log(storyCont);
	var content = storyObj.storyContent;
	//console.log(content);
	var pgsLoaded = false;


	//create first page
	//get storyText p
	//append storyObj content to it
	//insert it into the DOM
	//keep adding content until isOverflowed returns true
	//remove overflowed content
	//split content array element on space
	//add each word from the new array until isOverflowed is true
	//remove last word from storyText
	//keep track of storyContent array index
	//while the last content element hasn't been reached,
	//repeat the process (i.e. create a new page and assign content to it)
	//if, while processing the final storyContent array element
	//some content is left over, create a final page and put
	//all the remaining content there
	//if there is no more story content, stop the process

	var firstPage = createPage(storyObj.storyInfo);
	var storyText = getStoryText(firstPage);
	//console.log(firstPage);
	//console.log(storyText);

	var overflowed = false;
	var i = 0;
	addHtml(storyText, content[i]);
	storyCont.appendChild(firstPage);
	var currentHtml = "";

	while (!overflowed) {
		if (isOverflowed(storyText)) {
			overflowed = true;
			//console.log("overflowed");
			//console.log(storyText);
			break;
		}
		//console.log("still room");
		i++;
		//var iHtml = getInnerHtml(storyText);
		currentHtml = storyText.innerHTML;
		storyText.innerHTML = storyText.innerHTML + '<br><br>' + content[i];
	}

	//storyText is overflowed now - run second pass
	storyText.innerHTML = currentHtml + '<br><br>';
	overflowed = false;
	var wordArray = content[i].split(" ");
	//console.log(wordArray);
	var j = 0;
	storyText.innerHTML = storyText.innerHTML + " " + wordArray[j];	

	while (!overflowed) {
		if (isOverflowed(storyText)) {
			overflowed = true;
			//console.log("overflowed");
			//console.log(storyText);
			break;
		}
		j++;
		currentHtml = storyText.innerHTML;
		storyText.innerHTML = storyText.innerHTML + " " + wordArray[j];	
	}

	//handle word array with care!

	storyText.innerHTML = currentHtml;
	//wordArray[j] currently holds the word just removed

	//while there is content remaining, create new pages
	//and fill them with content.
	//faux edge-cases are handling the remainder of the word array 
	//and dealing with any content that doesn't fit 

	//only create a new page the current page has been
	//completely filled with content (i.e. overflowed is true)

	var addPage = true;
	var newPage;

	//handle word array
	if (j < wordArray.length) {
		//create new page 
		newPage = createPage();
		storyText = getStoryText(newPage);
		storyCont.appendChild(newPage);
		addPage = false;
		overflowed = false;

		while (j < wordArray.length && !overflowed) {
			if (isOverflowed(storyText)) {
				overflowed = true;
				//console.log("overflowed");
				//console.log(storyText);
				break;
			}
			currentHtml = storyText.innerHTML;
			storyText.innerHTML = storyText.innerHTML + " " + wordArray[j];	
			j++;
	  }
		//still need to check for overflow 
		//because it's possible a single array element
		//, even the remainder, has enough words
		//to fill up
		if (j === wordArray.length) {
			//storyText.innerHTML = currentHtml;
		} else if (overflowed) {
			//keep creating pages until no more overflow
			//will implement this later, as it does
			//not apply to my case
		}
	}

	//clear word array to make sure loop invariant is correct
	//console.log(wordArray);
	wordArray = null;

	//add remaining content (i.e. non-first-pages)
	while (i < content.length) {
		if (addPage === true) {
			newPage = createPage();
			storyText = getStoryText(newPage);
			storyCont.appendChild(newPage);
			addPage = false;
			overflowed = false;
		}

		//handle word array (which means we've added a new page)
		if (wordArray) {
			//add words
			//set wordArray to null
			while (j < wordArray.length) {
				if (isOverflowed(storyText)) {
					overflowed = true;
					//console.log("overflowed");
					//console.log(storyText);
					break;
				}
				currentHtml = storyText.innerHTML;
				storyText.innerHTML = storyText.innerHTML + " " + wordArray[j];	
				j++;
	  		}
		  		if (j === wordArray.length) {
					wordArray = null;
				} else if (overflowed) {
					//handle later - still words remaining in word array
				}
				//technically need to check if the remainder word array
				//content has overflowed the page
		}


		//need to check if i < content.length still
		while (!overflowed && i < content.length) {
			if (isOverflowed(storyText)) {
				overflowed = true;
				//console.log("overflowed");
				//console.log(storyText);
				break;
			}
			//console.log("still room");
			i++;
			//var iHtml = getInnerHtml(storyText);
			currentHtml = storyText.innerHTML;
			if (i < content.length) {
				storyText.innerHTML = storyText.innerHTML + '<br><br>' + content[i];
			}
			//console.log(storyText.innerHTML);
			//console.log(content[i]);
		}

		if (i >= content.length && !overflowed) {
			break; //we're done!
		} else if (i >= content.length) {
			break; //abnormal behavior -need to check for overflow
		}

		//what happens if i < content.length and there's no overflow? nothing(confirmed)

		//storyText is overflowed now and we haven't reached
		//the end of the content array - run second pass
		storyText.innerHTML = currentHtml + '<br><br>';
		overflowed = false;
		//console.log(content[i]);
	    wordArray = content[i].split(" "); //bug happens here if in beginning
	    //console.log(wordArray);
	    // of loop "wordArray else if (overflow)" isn't handled
		//console.log(wordArray);
	    j = 0;
		storyText.innerHTML = storyText.innerHTML + " " + wordArray[j];	

		while (!overflowed) {
			if (isOverflowed(storyText)) {
				overflowed = true;
				//console.log("overflowed");
				//console.log(storyText);
				break;
			}
			j++;
			currentHtml = storyText.innerHTML;
			storyText.innerHTML = storyText.innerHTML + " " + wordArray[j];	
		}

		//no need to check if j < wordArray.length because 
		//an overflow occured, which means there are still 
		//words remaining in our wordArray which need to be added
		//to a new page
		if (overflowed) {
			storyText.innerHTML = currentHtml;
			addPage = true;
		}
		//need to start the loop by adding any remaining words from
		//wordArray to the new page
	}

	pgsLoaded = true;


	function addText(elem, text) {
		var t = document.createTextNode(text);
		elem.appendChild(t);
	}

	function addHtml(elem, html) {
		elem.innerHTML = html;
	}

	function replaceText(elem, text) {
		var tN = elem.childNodes[0];
		tN.nodeValue = text;
	}

	function getTextNode(elem) {
		return elem.childNodes[0];
	}

	function getInnerHtml(elem) {
		return elem.innerHTML;
	}

	function getStoryText(page) {
		return page.getElementsByClassName("jh-story-text")[0];
	}

	function isOverflowed(element) {
    	return element.scrollHeight > element.clientHeight;
	}

	function createPage(storyInfo) {
		var page = document.createElement("div");
			page.className = "jh-story-page";
			var contentContainer = document.createElement("div");
				contentContainer.className = "jh-page-content-container";
				var storyText = document.createElement("p");
				storyText.className = "jh-story-text";

				if (storyInfo) {

					//update: added content close option
					if (returnUrl){
						var close = document.createElement("a");
							close.id = "jh-story-close";
							close.href = returnUrl;
							close.target = '_self';
							close.className = "jh-x3 pull-right";
							close.innerHTML =  '&#9587;'

						//var closeVal = document.createTextNode('&#9587;');

						//close.appendChild(closeVal);
						page.appendChild(close);
					}

					var title = document.createElement("h1");
						title.id = "jh-story-title";
					var author = document.createElement("h5");
						author.id = "jh-story-author";
						author.className = "jh-story-info";
					var pubDate = document.createElement("h6");
						pubDate.id = "jh-story-date"
						pubDate.className = "jh-story-info";

					storyText.className += " jh-story-p-first";

					//create text nodes for title, author, and date
					var titleVal = document.createTextNode(storyInfo.title);
					var authorVal = document.createTextNode(storyInfo.author);
					var pubDateVal = document.createTextNode(formatDate(storyInfo.date));

					//add text nodes to title, author, and date
					title.appendChild(titleVal);
					author.appendChild(authorVal);
					pubDate.appendChild(pubDateVal);

					//append title, author, and pubDate to page
					page.appendChild(title);
					page.appendChild(author);
					page.appendChild(pubDate);

		
				} else {
					storyText.className += " jh-story-p-not-first";
				}

			//append elements
			contentContainer.appendChild(storyText);
			page.appendChild(contentContainer);

			//clear out memory

			return page;
	}

	//pagination is finished
	callback(pgsLoaded);

}
