/*JSON Query Language */
/*Auxilliary methods for querying JSON objects */

module.exports = {

	findById: function(obj, id) {
		if (obj && id) {
			var lwrId = id.toLowerCase();
			var filtElems = obj.filter(function(elem) {
				if (elem.id) {
					return elem.id.toLowerCase() === lwrId;
				}
			});

			if (filtElems.length > 0) {
				return filtElems[0];
			}
			
		}
		//return an empty object
		return {};
	},
	isEmptyObject: function(obj) {
		for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    	}

    	return true && JSON.stringify(obj) === JSON.stringify({});
	}
}