/**
 * AchievementsController
 *
 * @description :: Server-side logic for managing Achievements
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var fs = require('fs');
var jsonql = require(__dirname + '/../../helpers/jsonql');

module.exports = {
	index: function(req, res) {
 		//console.log(req.param('name'));
 		var achvId = req.param('name');

 		if (achvId) {
 				sails.logger.info('[achievements] url: ' + req.url);
 				var path = __dirname + '/../../content/achievements.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						sails.logger.error('[achievements] error: ' + err);
 						//console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
 					var achievements = JSON.parse(data);
 					var achievement = jsonql.findById(achievements, achvId);

 					//console.log(JSON.stringify(achievement));

 					var recExists = !jsonql.isEmptyObject(achievement);

 					if (recExists) {
 						res.view();
 					} else {
 						sails.logger.error('[achievements] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			sails.logger.error('[achievements] error: no story id provided; url: ' + req.url);
 			res.notFound();
 		}

	},

	find: function(req, res) {
	if (req.wantsJSON) {
		sails.logger.info('[achievements - find] url: ' + req.url);
	  	//console.log('find');
 		//console.log(req.param('name'));
 		var achvId = req.param('name');

 		if (achvId) {
 				var path = __dirname + '/../../content/achievements.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						sails.logger.error('[achievements - find] error: ' + err);
 						//console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
 					var achievements = JSON.parse(data);
 					var achievement = jsonql.findById(achievements, achvId);

 					//console.log(JSON.stringify(achievement));

 					res.json(achievement);

 				});
 		} else {
 			sails.logger.error('[achievements - find] error: no story id provided; url: ' + req.url);
 		}
 	  } else {
 	  		sails.logger.error('[achievements - find: json required] url: ' + req.url + '; msg: html request to json endpoint');
 	  }
	}
}
