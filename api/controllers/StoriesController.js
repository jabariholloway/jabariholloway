/**
 * StoriesController
 *
 * @description :: Server-side logic for managing stories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var fs = require('fs');
 var jsonql = require(__dirname + '/../../helpers/jsonql');

 module.exports = {
 	index: function(req, res) {
 		var storyId = req.param('name');

 		if (storyId) {
				storyId = storyId.toLowerCase();
 				sails.logger.info('[stories] url: ' + req.url);
 				var path = __dirname + '/../../content/stories.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						sails.logger.error('[stories] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var stories = JSON.parse(data);
 					var story = jsonql.findById(stories, storyId);

 					//console.log(JSON.stringify(story));

 					//check if story is available - if so 

 					var recExists = !jsonql.isEmptyObject(story);

 					if (recExists) {
 						if (story.available === true) {
 						res.view();
 						} else {
 							res.redirect('/coming-soon');
 						}
 					} else {
 						sails.logger.error('[stories] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			sails.logger.error('[stories] error: no story id provided; url: ' + req.url);
 			res.notFound();
 		}

	},

	find: function(req, res) {
	if (req.wantsJSON) {
		sails.logger.info('[stories - find] url: ' + req.url);
		//get the story content (e.g. "content/music-major-graduating-with-dis-honors.json")
	    //console.log('find');
 		//console.log(req.param('name'));
 		var storyId = req.param('name');

 		if (storyId) {
				storyId = storyId.toLowerCase();
 				var infoPath = __dirname + '/../../content/Stories/' + storyId + '.json';
 				var contentPath = __dirname + '/../../content/Stories/' + storyId + '.txt';

 				fs.readFile(infoPath, function(err, data) {
 					if (err) {
 						sails.logger.error('[stories - find] error: ' + err);
 						//console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
 					var storyInfo = JSON.parse(data);

 					//get story content
 					fs.readFile(contentPath, 'utf8', function(err, data) {
	 					if (err) {
	 						sails.logger.error('[stories - content] error: ' + err);
	 						//console.log(err);
	 						res.serverError();
	 					}
	 					//need to query json object for id that matches achvName
	 					//if no match, 404 error
	 					//format data into an array of paragraphs
	 					//console.log(data);
	 					var storyContent = data.split("\n\n");
	 					/*console.log(data);
	 					console.log(storyContent);
	 					console.log(storyContent.length);*/

	 					var storyObj = { "storyInfo" : storyInfo, 
	 									"storyContent" : storyContent }

	 					res.json(storyObj);

	 					//storyInfo = null;

 					});

 				});
 		}
 		else {
 			sails.logger.error('[stories - find] error: no story id provided; url: ' + req.url);
 		}
 	  } else {
 	  		sails.logger.error('[stories - find: json required] url: ' + req.url + '; msg: html request to json endpoint');
 	  }
	}
 }
