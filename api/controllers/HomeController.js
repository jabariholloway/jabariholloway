/**
 * HomeController
 *
 * @description :: Server-side logic for managing Home
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var fs = require('fs');
var path = require('path');
var request = require('request');
var https = require('https');
var nodemailer = require('nodemailer');
var helper = require(__dirname + '/../../helpers/helper');
var appDir = path.dirname(require.main.filename);

module.exports = {

	index: function(req, res) {
		sails.logger.info('[html] url: ' + req.url);
		console.log(req.ip);
		//Use cookies later on when website becomes popular
		/*console.log(JSON.stringify(req.cookies));
		if (req.cookies.beenhere != '1') {
			var months = 1;
			var millisecs = months * 30 * 24 * 60 * 60 * 1000;
			res.cookie('beenhere','1', { maxAge: millisecs, httpOnly: true });
			sails.logger.info('[home] - Added "beenhere" cookie; expires in ' + months + ' month(s)');
			res.redirect('/#about');
		} else {
			res.view();
		}*/
		//get been-here cookie
		//if been-here cookie value is 0, 
		//	set been-here cookie value to 1 & expire every 1 month
		//	log cookie
		//  redirect to /#about
		//else return view
		res.view();

	},
	achievements: function(req, res) {
		if (req.wantsJSON) {
			sails.logger.info(' [home - achievements: json] url: ' + req.url);
			var path = __dirname + '/../../content/achievements.json';

			fs.readFile(path, function(err, data) {
				if (err) {
					//console.log(err);
					sails.logger.error(' [home - achievements: json] url: ' + req.url + '; error: ' + err);
					res.servorError();
				}
				res.json(JSON.parse(data));
			});
		 }
		else {
			sails.logger.error(' [home - achievements: json required] url: ' + req.url + '; msg: html request to json endpoint');
			res.redirect('/');
		}
	},
	stories: function(req, res) {
		if (req.wantsJSON) {
			sails.logger.info(' [home - stories: json] url: ' + req.url);
			var path = __dirname + '/../../content/stories.json';

			fs.readFile(path, function(err, data) {
				if (err) {
					console.log(err);
					sails.logger.error(' [home - stories: json] url: ' + req.url + '; error: ' + err);
					res.serverError();
				}
				res.json(JSON.parse(data));
			});
		}
		else {
			sails.logger.error(' [home - stories: json required] url: ' + req.url + '; msg: html request to json endpoint');
			res.redirect('/');
		}
	},
	subscribe: function(req, res) {

		if (req.param('email') && req.param('firstName') && req.param('lastName')) {
			sails.logger.info('[subscribe] url: ' + req.url);

				//define data params
				var email = req.param('email');
				var firstName = req.param('firstName');
				var lastName = req.param('lastName');

				sails.logger.info('[subscribe] name: ' + firstName + ' ' + lastName);
				sails.logger.info('[subscribe] email: ' + email);


				//ignoring subscribers' location information for now
				var query = JSON.stringify({
							 "email_address" : email,
							 "status" : "subscribed",
							 "status_if_new" : "subscribed",
							 "merge_fields" : { "FNAME" : firstName, "LNAME" : lastName },
							 "vip" : true
							});


					//console.log("host : " + sails.config.jh.MailChimpHost);
					//console.log("path : " + sails.config.jh.MailChimpSubscribePath);
					//console.log("api key : " + sails.config.jh.MailChimpAPIKey);

					var hash = helper.md5(email.toLowerCase());

				var options = {
					host: sails.config.jh.MailChimpHost,
					path: sails.config.jh.MailChimpSubscribePath + '/' + hash,
					method: 'PUT',
					headers: {
						"Authorization" : sails.config.jh.MailChimpAPIKey,
						"Content-Type" : "application/json",
						"Content-Length" : query.length
					}
				}

				var hreq = https.request(options, function(hres) {
					//console.log('Status Code: ' + hres.statusCode);
					//console.log('Headers: ' + JSON.stringify(hres.headers));
					sails.logger.info('[subscribe] status-code: ' + hres.statusCode);
					

					hres.setEncoding('utf8');

					hres.on('data', function(chunk) {
						console.log('\n\nCHUNK');
						console.log(chunk);
					});

					hres.on('end', function(end) {
						console.log('\n\nEND');
						res.send("success");
					});

					hres.on('error', function(err) {
						sails.logger.error('[subscribe] error: ' + err + '; url: ' + req.url);
						sails.logger.error('[subscribe] headers: ' + hres.headers);
						//console.log('Error: ' + err.message);
					});
				});

				hreq.write(query);
				hreq.end();

		}
		else {
			var err = 'Invalid subscription arguments';
			sails.logger.error('[subscribe] error: ' + err + '; url: ' + req.url);
			res.serverError(err);
		}



	},
	contact: function(req, res) {
		if (req.param('email') && req.param('name') && req.param('message')) {
			sails.logger.info('[contact-me] url: ' + req.url);

			//define data params
				//check if email is valid
				var email = req.param('email');
				var name = req.param('name');
				var msg = req.param('message');
				var contactEmail = 'jabari@jabariholloway.com';

				sails.logger.info('[contact-me] name: ' + name);
				sails.logger.info('[contact-me] email: ' + email);

				var respond = function(response) {
					sails.logger.info('[contact-me] response: ' + response);
					res.send(response);
				}

				var from = {
							name : 'jabariholloway.com',
							address : contactEmail
				}


				var subject = '[JabariHolloway.com] A message from ' + name;

				//send email
				helper.mail(from, contactEmail, email, subject, msg, respond);

		} else {
			var err = 'Invalid contact-me arguments';
			res.serverError(err);
			sails.logger.error('[contact-me] error: ' + err + '; url: ' + req.url);
		}

	},
	
 	comingSoon: function(req, res) {
		res.view();
	}	
};

