/**
 * LinkController
 *
 * @description :: Server-side logic for managing a link request
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 var fs = require('fs');
 var jsonql = require(__dirname + '/../../helpers/jsonql');

 module.exports = {
 	index: function(req, res) {
 		var linkId = req.param('name');

 		if (linkId) {
				linkId = linkId.toLowerCase();
 				sails.logger.info('[link] url: ' + req.url);
 				sails.logger.info('[link] ip: ' + req.ip);

 				var path = __dirname + '/../../content/links.json';

 				fs.readFile(path, function(err, data) {
 					if (err) {
 						sails.logger.error('[links] error: ' + err);
 						console.log(err);
 						res.serverError();
 					}
 					//need to query json object for id that matches achvName
 					//if no match, 404 error
					//console.log("test");
					//console.log(data.toString());
 					var links = JSON.parse(data);
 					var link = jsonql.findById(links, linkId);

 					//console.log(JSON.stringify(story));

 					//check if story is available - if so 

 					var recExists = !jsonql.isEmptyObject(link);

 					if (recExists) {
 						
 							res.redirect(link.url);
 						
 					} else {
 						sails.logger.error('[link] error: no record found; url: ' + req.url);
 						res.notFound();
 					}

 				});
 		} else {
 			sails.logger.error('[stories] error: no story id provided; url: ' + req.url);
 			res.notFound();
 		}

	}
 }
