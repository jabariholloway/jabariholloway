/**
 * ErrorController
 *
 * @description :: Server-side logic for viewing error pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */



 module.exports = {

 	index: function(req, res) {

 		var errorCode = req.param('id');

 		if (errorCode) {
 			if (errorCode === '404') {
 				res.notFound();
 			}
 			if (errorCode === '403') {
 				res.forbidden("Oops, you can't go here");
 			}
 			if (errorCode === '500') {
 				res.serverError('Something went wrong')
 			}
 			res.notFound();
 		} else {
 			res.notFound();
 		}
 	}

 }